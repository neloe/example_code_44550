#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
    const int ARRAY_SIZE = 5;
    int my_array[ARRAY_SIZE];
    for (int i=0; i<ARRAY_SIZE; i++)
        my_array[i] = 5;
    for (int i=0; i<ARRAY_SIZE; i++)
        cout << my_array[i] * i << " ";
    cout << endl;
    return 0;
}