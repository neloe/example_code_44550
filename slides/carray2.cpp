#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
    double another_array[4] = {1.0, 2.0, 3.0, 4.0};
    // note use of range based for loop
    for (auto i: another_array) cout << i << " ";
    cout << endl;
    int and_another[] = {1, 2, 3, 4, 5};
    for (auto i: and_another) cout << i << " ";
    cout << endl;
    // size is 5
    int one_more[4] = {1, 2};
    // contains [1, 2, 0, 0]
    for (auto i: one_more) cout << i << " ";
    cout << endl;
    return 0;
}