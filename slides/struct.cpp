#include <iostream>
using namespace std;

//Could go in a .h and be #included
struct point3d
{
    double m_x, m_y, m_z;
};

int main()
{
    point3d p1;
    p1.m_x = 3;
    p1.m_y = 4;
    p1.m_z = 5;

    point3d p2={1,2,3};
    //expected output: 325
    cout << p1.m_x << p2.m_y << p1.m_z << endl;
    return 0;
}