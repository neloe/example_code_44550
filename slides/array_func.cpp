double average(const double arr[], const int size)
{
    double sum = 0.0;
    for (int i=0; i<size; i++)
        sum += arr[i];
    return sum / size;
}

int main(int argc, char* argv[])
{
    double arr[] = {1.1, 2.2, 3.3, 4.4};
    average(arr, 4); //could print?
    return 0;
}