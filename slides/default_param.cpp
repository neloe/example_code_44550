#include <iostream>

using namespace std;

double size(double l, double w=1, double h=1);

int main()
{
    cout << size(4)  << " " << size(2, 3) << " "
         << size(4, 5, 6) << endl;
    return 0;
}

double size(double l, double w, double h)
{
    return l * w * h;
}