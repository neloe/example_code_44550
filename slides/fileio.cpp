#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));
    ofstream fout;
    int numtimes = rand() % 10;
    fout.open("randomdata.dat");
    for (int i=0; i<numtimes+1; i++)
        fout << rand() % 100 << " ";
    fout.close();
    // another way to open the stream, w/ constructor
    ifstream fin("randomdata.dat");
    int input;
    while (fin >> input) //fails at eof!
        cout << input << " ";
    cout << endl;
    fin.close();
    return 0;
}