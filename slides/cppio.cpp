#include <iostream>
#include <string>
using namespace std;

int main()
{
    string name, word;
    int a;
    double b;
    cout << "Enter a word: ";
    cin >> word;
    cout << "Enter an int and a double in that order: ";
    cin >> a >> b;
    cout << "Enter your name: ";
    //skip over the next 100 characters, or until the
    // next newline, whichever comes first
    cin.ignore(100, '\n');
    getline(cin, name);
    cout << a << " " << b << " " << word << " " 
         << name << endl;
    return 0;
}