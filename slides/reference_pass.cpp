#include <iostream>
using namespace std;
void swap(int & a, int & b);
void badswap (int a, int b);
void printvals(int a, int b);

int main()
{
    int a=4, b=5;
    printvals(a, b);
    swap(a, b);
    printvals(a, b);
    badswap(a, b);
    printvals(a, b);
    return 0;
}

void printvals(int a, int b)
{
    cout << a << " " << b << endl;
}

void swap(int & a, int & b)
{
    int temp = a;
    a = b;
    b = temp;
    return;
}

void badswap(int a, int b)
{
    int temp = a;
    a = b;
    b = temp;
    return;
}