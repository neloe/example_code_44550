#include "template.h"
template <class T>
double average(const T array[], const int size)
{
    T sum = 0;
    for (int i=0; i<size; i++)
        sum += array[i];
    return sum / static_cast<double>(size);
}