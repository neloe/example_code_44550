#include <iostream>
#include "template.h"

using namespace std;

int main()
{
    double arr1 [] = {1, 2, 3, 4, 5, 6};
    int arr2 [] = {2, 3, 4, 5, 6, 7};
    cout << average(arr1, 6) << " " << average(arr2, 6) << endl;
    return 0;
}