/*
 * File: template.h
 * Author: Nathan Eloe
 * Brief: A simple templated function
 */

 #ifndef TEMPLATE_H
 #define TEMPLATE_H
/// \brief: Calculates average of a set of values
/// \pre: operator + (T, T) is defined
/// \pre: operator / (T, double) is defined
/// \pre: size > 0
/// \post:
/// \return: average of the values
template <class T>
double average(const T array[], const int size);

// THIS LINE IS IMPORTANT!
#include "template.hpp"

#endif