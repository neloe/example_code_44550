#include <iostream>
using namespace std;

int fib_seq();

int main()
{
    for (int i=0; i<5; i++)
        cout << fib_seq() << " ";
    cout << endl;
    return 0;
}

int fib_seq()
{
    static int f0 = 1;
    static int f1 = 1;
    int fnew = f0 + f1;
    f0 = f1;
    f1 = fnew;
    return fnew;
}