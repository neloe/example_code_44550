#ifndef RATIONAL_H
#define RATIONAL_H

class Rational 
{
    private:
        int m_n, m_d;
    public:
        // Combined constructor with default constructors
        Rational(int n=0, int d=1): m_n(n), m_d(d) {}
        double value() const;
};

#endif