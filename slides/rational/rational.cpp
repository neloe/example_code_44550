#include "rational.h"

double Rational::value() const
{
    return m_n / static_cast<double>(m_d);
}

Rational & Rational::operator *=(const Rational & r)
{
    m_n *= r.m_n;
    m_d *= r.m_d;
    return *this;
}

Rational operator *(const Rational & lhs, const Rational & rhs)
{
    Rational result = lhs;
    result *= rhs;
    return result;
}