#include <iostream>
#include "multi_rational.h"
using namespace std;
int main()
{
    Rational r, r1(3), r2(22, 7);
    cout << r.value() << " " << r1.value() << " " 
         << r2.value() << endl;
    return 0;
}