#include "multi_rational.h"

double Rational::value() const
{
    return m_n/static_cast<double>(m_d);
}