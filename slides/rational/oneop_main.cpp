#include "rational.h"
#include <iostream>

using namespace std;

int main()
{
    Rational r(2, 3), r1(3, 4), r2(4,5);
    r *= r1;
    // should output 0.5 (2.0 / 4.0)
    cout << r.value() << endl;
    return 0;
}