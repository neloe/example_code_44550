#include "rational.h"
#include <iostream>

using namespace std;

int main()
{
    Rational r(2, 3), r1(3, 4), r2(4,5), r3;
    r *= r1;
    // should output 0.5 (2.0 / 4.0)
    cout << r.value() << endl;
    r3 = r1 * r2;
    // should output 0.6 (3.0 / 5.0)
    cout << r3.value() << endl;
    return 0;
}