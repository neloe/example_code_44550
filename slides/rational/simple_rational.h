#ifndef RATIONAL_H
#define RATIONAL_H

class Rational 
{
    private:
        int m_n, m_d;
    public:
        // Default Constructor
        Rational(){m_n = 0; m_d = 1;}
        Rational(int n): m_n(n), m_d(1) {}
        Rational(int n, int d): m_n(n), m_d(d) {}
        double value() const
        {
            return m_n / static_cast<double>(m_d);
        }
};

#endif