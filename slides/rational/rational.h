#ifndef RATIONAL_H
#define RATIONAL_H

class Rational 
{
    private:
        int m_n, m_d;
    public:
        Rational(int n=0, int d=1): m_n(n), m_d(d) {}
        double value() const;
        // In place Multiplication (binary operator)
        Rational & operator *=(const Rational & r);
        //friend defines a non-member function with access to
        // this class' private information
        friend Rational operator* (const Rational& lhs,
                                   const Rational& rhs);
};

#endif