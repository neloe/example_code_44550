#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{
    char fname[4] = {'B','o','b'};
    char lname [] = "this works!";
    char other [10];
    other[0] = 5; // whatever ascii character 5 is
    other[1] = 'b';
    other[2] = 0;
    // and so on...
    cout << fname << " - " << lname << " - " << other << endl;
    return 0;
}