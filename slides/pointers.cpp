#include <iostream>

using namespace std;

int main()
{
    int value = 5;
    int * ptr;
    // &value is the address of the variable
    ptr = &value;
    cout << "Value is: " << value << "; val pointed at is: " 
         << *ptr << endl;
    value = 10;
    cout << "Value is: " << value << "; val pointed at is: " 
         << *ptr << endl;
    cout << "Address of value is:     " << &value << endl;
    cout << "Value of the poitner is: " << ptr << endl;
    return 0;
}