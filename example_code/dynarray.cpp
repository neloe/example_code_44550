#include <iostream>

using namespace std;

void print_array(const int * array, const int size);

int main()
{
    const int ARRAY_SIZE = 10, ARRAY_SIZE2 = 15;
    int * my_array = new int[ARRAY_SIZE];
    for (int i=0; i<ARRAY_SIZE; i++)
        my_array[i] = (i + 1) * 2;
    print_array(my_array, ARRAY_SIZE);
    int * temp_array = new int[ARRAY_SIZE2];
    for (int i=0; i<ARRAY_SIZE; i++)
        temp_array[i] = my_array[i];
    // FIRST: free old array
    delete[] my_array;
    // SECOND: point array at new array
    my_array = temp_array;
    // THIRD: do NOT delete temp_array at this point...
    // doing so will also delete the current array
    for (int i=ARRAY_SIZE; i<ARRAY_SIZE2; i++)
        my_array[i] = (i+1) * 2;
    print_array(my_array, ARRAY_SIZE2);
    // NOW we free one of the arrays
    delete[] my_array;
    return 0;

}

void print_array(const int * array, const int size)
{
    for (int i=0; i<size; i++)
        cout << array[i] << " ";
    cout << endl;
}