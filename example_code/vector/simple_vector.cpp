#include "simple_vector.h"
#include <stdexcept>
using namespace std;

simple_vector::~simple_vector()
{
    // Nifty way to check for null
    if (m_data)
        delete[] m_data;
    // set it to null just in case
    m_data = 0;
}

void simple_vector::double_size()
{
    int * temp = m_data;
    if (capacity == 0)
        capacity = 1;
    capacity *= 2;
    m_data = new int[capacity];
    for (unsigned int i=0; i<size; i++)
        m_data[i] = temp[i];
    // free the old array
    if(temp)
	    delete[] temp;
}

void simple_vector::push_back(const int data)
{
    if (size == capacity)
        double_size();
    m_data[size] = data;
    size++;
}

const int & simple_vector::operator[] (const unsigned int idx) const
{
    if (idx >= size)
        throw out_of_range("Index is too large");
    return m_data[idx];
}

int & simple_vector::operator[] (const unsigned int idx)
{
    if (idx >= size)
        throw out_of_range("Index is too large");
    return m_data[idx];
}
