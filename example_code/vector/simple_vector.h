/*
* File: simple_vector.h
* Author: Nathan Eloe
* Brief: A simple vector class to demonstrate dynamic memory managemet in C++
*/
#ifndef SIMPLE_VECTOR_H
#define SIMPLE_VECTOR_H

class simple_vector
{
    private:
        int * m_data;
        unsigned int size;
        unsigned int capacity;
        void double_size();
    public:
        simple_vector(): m_data(0), size(0), capacity(0) {}
        ~simple_vector();
        void push_back(const int data);
        const int & operator[] (const unsigned int idx) const;
        int & operator[] (const unsigned int idx);
};

#endif
