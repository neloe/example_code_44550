#include "template_vector.h"
#include <stdexcept>
using namespace std;

template <class T>
template_vector<T>::~template_vector()
{
    // Nifty way to check for null
    if (m_data)
        delete[] m_data;
    // set it to null just in case
    m_data = 0;
}

template <class T>
void template_vector<T>::double_size()
{
    T * temp = m_data;
    if (capacity == 0)
        capacity = 1;
    capacity *= 2;
    m_data = new T[capacity];
    for (unsigned int i=0; i<size; i++)
        m_data[i] = temp[i];
    // free the old array
    delete[] temp;
}

template <class T>
void template_vector<T>::push_back(const T data)
{
    if (size == capacity)
        double_size();
    m_data[size] = data;
    size++;
}

template <class T>
const T & template_vector<T>::operator[] (const unsigned int idx) const
{
    if (idx >= size)
        throw new out_of_range("Index is too large");
    return m_data[idx];
}

template <class T>
T & template_vector<T>::operator[] (const unsigned int idx)
{
    if (idx >= size)
        throw new out_of_range("Index is too large");
    return m_data[idx];
}