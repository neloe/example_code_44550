#include <iostream>
#include "template_vector.h"
using namespace std;

int main()
{
    const int ARRAY_SIZE=10;
    template_vector<int> v1;
    template_vector<double> v2;
    for (int i=0; i<ARRAY_SIZE; i++)
    {
        v1.push_back(i*2);
        v2.push_back(i*0.5);
    }
    for (int i=0; i<ARRAY_SIZE; i++)
        cout << v1[i] << " ";
    cout << endl;

    for (int i=0; i<ARRAY_SIZE; i++)
        cout << v2[i] << " ";
    cout << endl;

    return 0;
}