#include "simple_vector.h"
#include <iostream>

using namespace std;

int main()
{
    simple_vector v;
    for (int i=0; i<100; i++)
        v.push_back(i);
    for (int i=0; i<100; i++)
        cout << v[i] << " ";
    cout << endl;
    for (int i=0; i<100; i++)
        v[i] = 10;
    for (int i=0; i<100; i++)
        cout << v[i] << " ";
    cout << endl;
    return 0;
}
