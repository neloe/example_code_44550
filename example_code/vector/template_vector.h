/*
 * File: template_vector.h
 * Author: Nathan Eloe
 * Brief: A templated vector class to demonstrate dynamic memory managemet in C++
 */

#ifndef TEMPLATE_VECTOR_H
#define TEMPLATE_VECTOR_H

template <class T>
class template_vector
{
    private:
        T * m_data;
        unsigned int size;
        unsigned int capacity;
        void double_size();
    public:
        template_vector(): m_data(0), size(0), capacity(0) {}
        ~template_vector();
        void push_back(const T data);
        const T & operator[] (const unsigned int idx) const;
        T & operator[] (const unsigned int idx);
};

#include "template_vector.hpp"
#endif