#include <iostream>

using namespace std;

int main()
{
	int a;
	int b = 16;
	int * ptr = &a;
	//pointer to a pointer
	int ** ptrptr = &ptr;
	//printf("%d %d\n", *ptr, *(*ptrptr));
	cout << *ptr << " " << *ptrptr << " " << *(*ptrptr) << endl;
	*ptr = 59;
	*ptrptr = &b;
	//printf("%d %d\n", *ptr, a);
	cout << *ptr << " " << a << endl;
	return 0;
}
