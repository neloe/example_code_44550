#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <pthread.h>

using namespace std;

struct bitonic_arg {
    int * arr;
    int size;
    int direction;
};

pthread_t launch_thread(bitonic_arg * b);

/*
 * brief: sorts the first size integers in the array pointed to by arr
 *        using the bitonic sorting method in the specified direction
 *        see: https://en.wikipedia.org/wiki/Bitonic_sorter
 */
void* bitonic(void * arg);
/*
 * brief: performs the bitonic merge operation
 *        see: https://en.wikipedia.org/wiki/Bitonic_sorter
 */
void seq_bitonic_merge(const int direction, int *arr, const int size);
/*
 * brief: performs the bitonic comparison across the chunks
 */
void bitonic_compare(const int direction, int *arr, const int size);
/*
 * brief: prints size elements of the array pointed at by arr
 * pre: size < = size allocated for the array
 */
void print_array(const int *arr, const int size);
/*
 * brief: randomly fills size elements of the array with numbers [0, 100)
 * pre: size < = size allocated for the array
 */
void randofill(int *arr, const int size);

int main(int argc, char *argv[])
{
    srand(time(0));
    int numnums = 1 << atoi(argv[1]);
    //int numnums = 16;
    int * tosort = new int[numnums];
    void * status;

    bitonic_arg b;
    b.arr = tosort;
    b.size = numnums;
    b.direction=1;

    randofill(tosort, numnums);
    //print_array(tosort, numnums);
    pthread_t thread = launch_thread(&b);
    pthread_join(thread, &status);
    //print_array(tosort, numnums);
    delete tosort;
    return 0;
}

void randofill(int *arr, const int size)
{
    int i;
    for (i = 0; i < size; i++)
        arr[i] = rand() % 100;
}

pthread_t launch_thread (bitonic_arg * b)
{
    pthread_t th;
    pthread_create (&th, NULL, bitonic, (void*) b);
    return th;
}

void* bitonic(void * arg)
{
    bitonic_arg * bit = static_cast<bitonic_arg *>(arg);
    // this is initialization via an initialization list.
    // it's cool
    bitonic_arg h1 = {bit->arr, bit->size/2, 1};
    bitonic_arg h2 = {&(bit->arr[bit->size/2]), bit->size - bit->size/2, 0};
    pthread_t t1, t2;
    void * status;
    if (bit->size > 1)
    {
        t1 = launch_thread(&h1);
        t2 = launch_thread(&h2);
        pthread_join(t1, &status);
        pthread_join(t2, &status);
        seq_bitonic_merge(bit->direction, bit->arr, bit->size);
    }
    pthread_exit(NULL);
}

void seq_bitonic_merge(const int direction, int *arr, const int size)
{
    if (size > 1) {
        int *midpt = &arr[size / 2];
        bitonic_compare(direction, arr, size);
        seq_bitonic_merge(direction, arr, size / 2);
        seq_bitonic_merge(direction, midpt, size - size / 2);
    }
}
void bitonic_compare(const int direction, int *arr, const int size)
{
    int dist = size / 2;
    int i;
    for (i = 0; i < dist; i++) {
        if ((arr[i] > arr[i + dist]) == direction)
            swap(arr[i], arr[i + dist]);
    }
}

void print_array(const int *arr, const int size)
{
    // I am using printf here for a reason: I want nice formatting!
    //for (i = 0; i < size; i++)
    //    printf("%2.d ", arr[i]);
    cout.width(2);
    for (int i=0; i<size; i++)
        cout << arr[i] << " " ;
    cout << endl;
}
