/*!
 * \file smarter_bitonic.c
 * \brief Implements a smarter threaded bitonic sort
 * \author Nathan Eloe
 */
#include <iostream>
#include <algorithm>
#include <thread>

using namespace std;


/*
 * brief: sorts the array pointed to by arr using the bitonic sort.
 *        launches two threads to sort each of two halves of the array
 */
void magic_bitonic(const int direction, int * arr, const int size);
/*
 * brief: sorts the first size integers in the array pointed to by arr
 *        using the bitonic sorting method in the specified direction
 *        see: https://en.wikipedia.org/wiki/Bitonic_sorter
 */
void seq_bitonic(const int direction, int * arr, const int size);
/*
 * brief: performs the bitonic merge operation
 *        see: https://en.wikipedia.org/wiki/Bitonic_sorter
 */
void seq_bitonic_merge(const int direction, int * arr, const int size);
/*
 * brief: performs the bitonic comparison across the chunks
 */
void bitonic_compare(const int direction, int * arr, const int size);
/*
 * brief: prints size elements of the array pointed at by arr
 * pre: size < = size allocated for the array
 */
void print_array(const int * arr, const int size);
/*
 * brief: randomly fills size elements of the array with numbers [0, 100)
 * pre: size < = size allocated for the array
 */
void randofill(int * arr, const int size);

int main(int argc, char* argv[])
{
  srand(time(0));
  int numnums = 1 << atoi(argv[1]);
  int * tosort = new int[numnums];
  randofill(tosort, numnums);
  print_array(tosort, numnums);
  magic_bitonic(1, tosort, numnums);
  print_array(tosort, numnums);
  delete[] tosort;
  return 0;
}

void magic_bitonic(const int direction, int * arr, const int size)
{
    if (size <= 1) return;
    thread t1(seq_bitonic, 1, arr, size/2);
    thread t2(seq_bitonic, 0, &arr[size / 2], size - size/2);
    t1.join();
    t2.join();
    /*
    bitonic h1 = {arr, size / 2, 1};
    bitonic h2 = {&(arr[size / 2]), size - size / 2, 0};
    pthread_t th1, th2;
    void * status;
    if (size <= 1)
        return;
    th1 = launch_thread(&h1);
    th2 = launch_thread(&h2);
    pthread_join(th1, &status);
    pthread_join(th2, &status);
    */
    seq_bitonic_merge(direction, arr, size);
}

void randofill(int * arr, const int size)
{
  int i;
  for (i=0; i<size; i++)
    arr[i] = rand() % 100;
}


void seq_bitonic(const int direction, int * arr, const int size)
{
  if (size > 1)
  {
    int * midpt = &arr[size / 2];
    seq_bitonic(1, arr, size / 2);
    seq_bitonic(0, midpt, size - (size / 2));
    seq_bitonic_merge(direction, arr, size);
  }
}
void seq_bitonic_merge(const int direction, int * arr, const int size)
{
  if (size > 1)
  {
    int * midpt = &arr[size / 2];
    bitonic_compare(direction, arr, size);
    seq_bitonic_merge(direction, arr, size / 2);
    seq_bitonic_merge(direction, midpt, size - size / 2);
  }
}
void bitonic_compare(const int direction, int * arr, const int size)
{
  int dist = size / 2;
  int i;
  for (i=0; i<dist; i++)
  {
    if ((arr[i] > arr[i + dist]) == direction)
      swap(arr[i], arr[i+dist]);
  }
}

void print_array(const int * arr, const int size)
{
  cout.width(2);
  for (int i=0; i<size; i++)
	  cout << arr[i] << " ";
  cout << endl;
}
