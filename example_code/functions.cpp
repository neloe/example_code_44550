// functions.cpp
// Author Nathan Eloe
// Demonstration of basic principles of the C programming language

#include <iostream>

using namespace std;

void greet();
float avg(int a, int b);

int main(int argc, char* argv[])
{
    int val1 = 100, val2 = 50, average;
    greet();
    cout << "The average of " << val1 << " and " << val2 << " is " << avg(val1, val2) << endl;
    // or
    average = avg(val1, val2);
    cout << "The average of " << val1 << " and " << val2 << " is " << average << endl;
    return 0;
}


void greet()
{
    cout << "Hello user!" << endl;
    return;
}

float avg(int a, int b)
{
    return (a + b) / 2.0;
}
