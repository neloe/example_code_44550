/*
 * File: llist_ugh.cpp
 * Author: Nathan Eloe
 * Brief: A demonstration of pointers in C++
 * NOTE: THIS IS NOT A GOOD WAY TO DO LINKED LISTS DO NOT DO THIS WRITE A CLASS INSTEAD
 */
#include <iostream>

using namespace std;
struct llist
{
    int data;
    llist * next;
};

void printlist(const llist * list);

int main()
{
    int size = 0;
    llist * head;
    llist * tail;
    // Allocate memory for a single linked list node
    head = new llist;
    head -> data = 5;
    head -> next = NULL;
    tail = head;
    size ++;
    // let's push back;
    tail -> next = new llist;
    tail = tail->next;
    tail -> next = NULL;
    // let's dereference a different way
    (*tail).data = 10;
    printlist(head);
    cout << endl;
    //NOW: we must clean up the memory
    // easy because we have two nodes; with more you have to loop through
    // Try commenting out these two lines and running it through valgrind!
    //delete tail;
    //delete head;
    // You also have to be careful to not double-free... if you uncomment this line bad juju happens
    //delete head;
    return 0;
}

void printlist(const llist * list)
{
    if (list == NULL)
        return;
    cout << list->data << " ";
    printlist(list->next);
}
