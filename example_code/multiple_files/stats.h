/*
 * File: stats.h
 * Author: Nathan Eloe
 * Brief: Some statistics functions
 */

#ifndef STATS_H
#define STATS_H

#include<vector>

/*
 * \pre:
 * \post:
 * \return: average of the data in the vector
 */
 // NOTE: Instead of using namespace std; here, I am specifying std::vector
 // This is better to do in your .h files when you can
double average(const std::vector<double> &data);

/*
 * \pre:
 * \post:
 * \return: standard deviation of the data in the vector
 */
 double std_dev(const std::vector<double> &data);

#endif