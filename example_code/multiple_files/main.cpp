/*
 * File: main.cpp
 * Author: Nathan Eloe
 * Brief: Demonstration of multiple files
 */
#include "stats.h"
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;
int main()
{
    srand(time(0));
    vector<double> mydata;
    // RAND_MAX is the largest value that rand() can return
    double randfactor = 1.0 / RAND_MAX;
    for (int i=0; i<1000; i++)
        mydata.push_back(rand() * randfactor);
    cout << "Average is: " << average(mydata) << endl;
    cout << "Standard Deviation is: " << std_dev(mydata) << endl;
    cout << "The uniform distribution ( on [0,1] ) should have a mean of 0.5  and a std deviation of "
         << 1.0 / sqrt(12.0) << endl;
    return 0;
}
