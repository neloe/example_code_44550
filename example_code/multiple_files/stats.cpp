/*
 * File: stats.cpp
 * Author: Nathan Eloe
 * Brief: Definition of some statistics functions
 */
#include "stats.h"
#include <cmath> // for sqrt
using namespace std;

double average(const vector<double> &data)
{
    // To avoid having a precondition, and for better error handling
    if (data.size() == 0)
        return 0;
    double sum = 0.0;
    for (const double d: data)
        sum += d;
    return sum / data.size();
}

double std_dev(const vector<double> &data)
{
    if (data.size() < 2)
        return 0;
    double mean = average(data);
    double sum = 0.0;
    double meandiff;
    for (const double d: data)
    {
        meandiff = d - mean;
        sum += meandiff * meandiff;
    }
    return sqrt(sum / (data.size() - 1));
}