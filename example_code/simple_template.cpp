// Example of simple templating
#include <iostream>
void swap(int & a, int & b)
{
	std::cout << "Special one!\n";
	int temp = a;
	a = b;
	b = temp;
	return;
}

template <class T>
void swap(T & a, T & b)
{
	T temp = a;
	a = b;
	b = temp;
	return;
}

int main()
{
	int a = 5, b = 6;
	double f1 = 3.14, f2 = 6.28;
	std::cout << a << " " << b << std::endl;
	swap(a, b);
	std::cout << a << " " << b << std::endl;
	std::cout << f1 << " " << f2 << std::endl;
	swap(f1, f2);
	std::cout << f1 << " " << f2 << std::endl;

	return 0;
}
