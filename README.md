# 44-550 (Operating Systems) Course Source Code

This repository contains all of the example code, source from slides, and in class demonstration code for the 44-550 (Operating Systems) class at Northwest Missouri State University.  All code can be built and run, and Makefiles are supplied to build all of the code.

By default, running `make` in the `slides/` and `example_code/` will build all targets in the top level of that directory.  Some multiple file examples have their own directories and will need to be built from those directories.  The `Makefile` lists all available targets that can be built separately.

In class demo code will be updated as the semesters progress, and code from previous semesters will remain in the repository.