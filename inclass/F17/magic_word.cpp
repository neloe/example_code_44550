#include <signal.h>
#include <iostream>

using namespace std;

void interruptingCow(int signum);

int main()
{
	signal(SIGINT, interruptingCow);
	while(true);
	return 0;
}

void interruptingCow(int signum)
{
	cout << "Ah ah ah, you didn't say the magic word!" << endl;
	while(true);
}
