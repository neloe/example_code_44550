#include <iostream>
using namespace std;
int main()
{
	const double CONV_FACTOR=5.0/9;
	double tempf = 32.0;
	
	cin >> tempf;
	//Bad things happen when you switch << and >> here
	//cin << tempf;

	double tempc = (tempf - 32.0) * CONV_FACTOR;

	cout << "Temp is " << tempc << endl;
	if (tempc < 0.0)
		cout << "HOLY CRAP IT IS COLD" << endl;
	else if (tempc > 100.1)
		cout << "that's pretty warm" << endl;
	else if (tempc < 99.9)
		cout << "magic stuff" << endl;
	else
		cout << "Water's boiling, let's make pasta" << endl;

	cout << "Let's make Hunter Happy!" << endl;
	int a = 5;
	cout << a << endl;
	cout << a++ << endl;
	cout << ++a << endl;
	return 0;
}
