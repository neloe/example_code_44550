#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <netdb.h>
using namespace std;

int main(int argc, char* argv[])
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
  int client;
  sockaddr_in server;
  hostent * host;
  char mesg[] = "Hello world!";
  char input[256];
  int size;

  host = gethostbyname(argv[1]);
  if(!host)
  {
    cerr << "Client error: 404" << endl;
    exit(1);
  }

  if (sock == -1)
  {
    cerr << "I am error: creating socket" << endl;
    exit(1);
  }
          

  server.sin_family = AF_INET;
  server.sin_port=htons(9999);
  memcpy(&server.sin_addr, host->h_addr, host->h_length);

  if (connect(sock, (const struct sockaddr*)&server, sizeof(sockaddr_in)) == -1)
  {
    cerr << "I am error: connecting" << endl;
    close(sock);
    exit(1);
  }

  write(sock, "Writing!", strlen("Writing!"));
  usleep(3);
  write(sock, (void*) mesg, strlen(mesg));
  read(sock, input, 256);
  cout << "Client got " << input << endl;
  cout << "Closing and exiting" << endl;
  close(sock);
  return 0;
}
