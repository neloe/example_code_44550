#include <iostream>
#include <unistd.h>
#include <signal.h>

using namespace std;

void batterAndUmp(int signum);

int main()
{
	signal(SIGUSR1, batterAndUmp);
	int cpid = fork();

	if (cpid == 0)
	{
		while(true);
	}
	else
	{
		for (int i=0; i<9; i++)
		{
			kill(cpid, SIGUSR1);
			sleep(1);
		}
		kill(cpid, SIGTERM);
	}
	return 0;
}
 
void batterAndUmp(int signum)
{
	static int pitches = 0;
	pitches++;
	cout << "STEEEERIKE " << pitches << endl;
	if (pitches == 3)
	{
		pitches = 0;
		cout << "Yer out!" << endl;
	}
}
