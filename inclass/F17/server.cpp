#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
using namespace std;

int main(int argc, char* argv[])
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
  int client;
  char data[256];
  int size;
  sockaddr_in my_address;
  my_address.sin_family = AF_INET;
  my_address.sin_port=htons(9999);
  my_address.sin_addr.s_addr = INADDR_ANY;

  if (sock == -1)
  {
    cerr << "I am error: creating socket" << endl;
    exit(1);
  };

  if(bind(sock, (sockaddr*)&my_address, sizeof(sockaddr_in)) == -1)
  {
    cerr << "I am error: binding socket" << endl;
    close(sock);
    exit(1);
  }

  if(listen(sock, 10) == -1)
  {
    cerr << "I am error: listen" << endl;
    close(sock);
    exit(1);
  }

  client = accept(sock, NULL, NULL);
  read(client, data, 256);
  cout << "Server got: " << data << endl;
  read(client, data, 256);
  cout << "Server got: " << data << endl;
  write(client, "Server exiting", strlen("Server exiting"));
  cout << "Server: All done, closing the sock"
       << endl;
  close(sock);

  return 0;
}
